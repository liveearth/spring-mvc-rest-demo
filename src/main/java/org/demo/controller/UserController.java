package org.demo.controller;

import java.util.List;

import org.demo.controller.dto.UserDTO;
import org.demo.exception.ErrorResponse;
import org.demo.exception.UserNotFoundException;
import org.demo.model.User;
import org.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@Autowired
	private UserService userService;

	@RequestMapping("/users")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/users")
	public User createUser(@RequestBody User user) {
		return userService.createUser(user);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/users")
	public User updateUser(@RequestBody User user) {
		return userService.updateUser(user);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/users/{email}")
	public void deleteUser(@PathVariable String email) {
		userService.deleteUser(email);
	}

	@RequestMapping(method = RequestMethod.POST, value="/login")
	public UserDTO login(@RequestBody User user) {
		return userService.loginUser(user);
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND.value(), "User not exist", ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.NOT_FOUND);
	}
}
