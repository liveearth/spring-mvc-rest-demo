package org.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "user_details")
public class User {
	@Id
	private String email;
	private String password;
	private String name;
	@Column(name = "last_login", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin;

	public User() {

	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getLastLogin() {
		return (lastLogin == null) ? null : new Date(lastLogin.getTime());
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = (lastLogin == null) ? null : new Date(lastLogin.getTime());
	}

	@Override
	public String toString() {
		return "User [email=" + email + ", password=" + password + ", name=" + name + ", lastLogin=" + lastLogin + "]";
	}
}
