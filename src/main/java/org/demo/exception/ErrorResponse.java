package org.demo.exception;

public class ErrorResponse {

	private int statusCode;
	private String message;
	private String debugMessage;

	public ErrorResponse(int statusCode, String message, String debugMessage) {
		super();
		this.statusCode = statusCode;
		this.message = message;
		this.debugMessage = debugMessage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDebugMessage() {
		return debugMessage;
	}

	public void setDebugMessage(String debugMessage) {
		this.debugMessage = debugMessage;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

}
